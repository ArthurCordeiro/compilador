package main;

import java.io.FileReader;

import java_cup.runtime.Symbol;

import files.parser.sym; // esse arquivo e gerado com o CUP
import files.scanner.scanner; //esse arquivo e gerado com o JFLEX


//Need to change ScannerMain to Scanner in all files
public class Scanner 
{

	public static void execute(String caminho) 
	{
		try 
		{
			// cria um scanner a partir de um arquivo de entrada
			scanner novo_scanner = new scanner(
					new FileReader(caminho));
			Symbol proximo_token = novo_scanner.next_token();
			

			while (proximo_token.sym != sym.EOF) 
			{
				// printa todo token que passa pelo scanner
				
				System.out.print(novo_scanner.symbolToString(proximo_token) + " ");
				proximo_token = novo_scanner.next_token();
			}
			System.out.print("\nLexical analysis completed");
		} catch (Exception e) 
		{
			System.err.println("Unexpected internal compiler error: "
					+ e.toString());
			
			e.printStackTrace();
		}
	}
}

