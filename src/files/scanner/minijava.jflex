/**
 * JFlex specification for lexical analysis of a simple demo language.
 * Change this into the scanner for your implementation of MiniJava.
 *
 * CSE 401/M501/P501 18sp
 */


package Scanner;

import java_cup.runtime.Symbol;
import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.ComplexSymbolFactory.ComplexSymbol;
import java_cup.runtime.ComplexSymbolFactory.Location;
import Parser.sym;

%%
%public
%final
%class scanner
%unicode
%cup
%line
%column

%{
  // note that these Symbol constructors are abusing the Symbol
  // interface to use Symbol's left and right fields as line and column
  // fields instead


  private Symbol symbol(int type) 
  {
    return new Symbol(type, yyline+1, yycolumn+1);
  }
  private Symbol symbol(int type, Object value) 
  {
    return new Symbol(type, yyline+1, yycolumn+1, value);
  }

  // print out a symbol (aka token) nicely
  public String symbolToString(Symbol s) 
  {
    switch (s.sym) 
    {
      case sym.BECOMES: return "BECOMES";
      case sym.SEMICOLON: return "SEMICOLON";
      case sym.PLUS: return "PLUS";
      case sym.LPAREN: return "LPAREN";
      case sym.RPAREN: return "RPAREN";
      case sym.RETURN: return "RETURN";
      case sym.CLASS:  return "CLASS";
      case sym.IDENTIFIER: return "ID(" + (String)s.value + ")";
      case sym.DIGIT: return "DIGIT(" + (Integer)s.value + ")";
      case sym.EOF: return "EOF";
      case sym.error: return "<ERROR>";
      case sym.LCHAVE: return "LCHAVE";
      case sym.RCHAVE: return "RCHAVE";
      case sym.LBRACKET: return "LBRACKET";
      case sym.RBRACKET: return "RBRACKET";
      case sym.DOT: return "DOT";
      case sym.PUBLIC: return "PUBLIC";
      case sym.STATIC: return "STATIC";
      case sym.VOID:   return "VOID";
      case sym.MAIN:   return "MAIN";
      case sym.STRING:   return "STRING";
      case sym.EXTENDS:   return "EXTENDS";
      case sym.INT:   return "INT";
      case sym.BOOL:   return "BOOL";
      case sym.IF:   return "IF";
      case sym.ELSE:   return "ELSE";
      case sym.WHILE:   return "WHILE";
      case sym.PRINT:   return "PRINT";
      case sym.LENGTH:   return "LENGTH";
      case sym.TRUE:   return "TRUE";
      case sym.FALSE:   return "FALSE";
      case sym.THIS:    return "THIS";
      case sym.NEW:     return "NEW";
      case sym.LESSTHAN:     return "LESSTHAN";
      case sym.ANDAND:     return "ANDAND";
      case sym.MINUS:     return "MINUS";
      case sym.STAR:     return "STAR";
      case sym.COMMA:  return "COMMA";
      case sym.NEG: return "NEG";
      default: return "<UNEXPECTED TOKEN " + s.toString() + ">";
    }
  }
%}

/* Helper definitions */
//letter = [a-zA-Z]
Identifier = [A-Za-z_][A-Za-z_0-9]*
Digit = (0 | [1-9][0-9]*)+
eol = [\r\n]
white = {eol}|[ \t\f]

//Comentários (Somente o "//")
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
EndOfLineComment = "//" {InputCharacter}* {LineTerminator}?

Comment = {EndOfLineComment}

/*%eofval
{
    return symbol(sym.EOF);
%eofval}*/

%%

/* Token definitions */

/* reserved words */
/* (put here so that reserved words take precedence over identifiers) */
"return" { return symbol(sym.RETURN); }
"class"  { return symbol(sym.CLASS);}
"public" { return symbol(sym.PUBLIC);}
"static" { return symbol(sym.STATIC);}
"void"   { return symbol(sym.VOID);}
"main"   { return symbol(sym.MAIN);}
"String" { return symbol(sym.STRING);}
"extends" { return symbol(sym.EXTENDS);}
"int"   { return symbol(sym.INT);}
"boolean" { return symbol(sym.BOOL);}
"if"    { return symbol(sym.IF);}
"else"    { return symbol(sym.ELSE);}
"while"   { return symbol(sym.WHILE);}
"System.out.println" { return symbol(sym.PRINT);}
"length"   { return symbol(sym.LENGTH);}
"true"     { return symbol(sym.TRUE);}
"false"    { return symbol(sym.FALSE);}
"this"     { return symbol(sym.THIS);}
"new"    { return symbol(sym.NEW);}

/* operators */
"+"  { return symbol(sym.PLUS); }
"="  { return symbol(sym.BECOMES); }
"<"  { return symbol(sym.LESSTHAN);}
"&&" { return symbol(sym.ANDAND);}  
"-"  { return symbol(sym.MINUS);}
"*"  { return symbol(sym.STAR);}
"!"  { return symbol(sym.NEG);}

/* delimiters */
"(" { return symbol(sym.LPAREN); }
")" { return symbol(sym.RPAREN); }
";" { return symbol(sym.SEMICOLON); }
"{" { return symbol(sym.LCHAVE); }
"}" { return symbol(sym.RCHAVE); }
"[" { return symbol(sym.LBRACKET);}
"]" { return symbol(sym.RBRACKET);}
"." { return symbol(sym.DOT);}
"," { return symbol(sym.COMMA);}

/* identifiers */
/*{letter} ({letter}|{digit}|_)* 
  { return symbol(sym.IDENTIFIER, yytext()); }*/

{Digit}      { return symbol(sym.DIGIT, new Integer(yytext())); }

{Identifier} { return symbol(sym.IDENTIFIER, yytext() ); }

{Comment}    { /* Faz Nada */ }

<<EOF>>      { return symbol(sym.EOF); }

/* whitespace */
{white}+ { /* ignore whitespace */ }

/* lexical errors (put last so other matches take precedence) */
. { throw new LexicalCompilerException(
  "unexpected character in input: '" + yytext() + "'", 
  yyline+1, yycolumn+1);
  }