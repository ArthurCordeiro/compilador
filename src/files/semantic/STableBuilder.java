/* alredy check*/


package files.semantic;

import java.util.*;


import AST.And;
import AST.ArrayAssign;
import AST.ArrayLength;
import AST.ArrayLookup;
import AST.Assign;
import AST.Block;
import AST.BooleanType;
import AST.Call;
import AST.ClassDeclExtends;
import AST.ClassDeclSimple;
import AST.Display;
import AST.DoubleType;
import AST.False;
import AST.Formal;
import AST.GreaterThan;
import AST.Identifier;
import AST.IdentifierExp;
import AST.IdentifierType;
import AST.If;
import AST.IfElse;
import AST.IntArrayType;
import AST.IntegerLiteral;
import AST.IntegerType;
import AST.LessThan;
import AST.MainClass;
import AST.MethodDecl;
import AST.Minus;
import AST.NewArray;
import AST.NewObject;
import AST.Not;
import AST.Or;
import AST.Plus;
import AST.Print;
import AST.Program;
import AST.This;
import AST.Times;
import AST.True;
import AST.Type;
import AST.VarDecl;
import AST.While;
import AST.Visitor.Visitor;

public class STableBuilder implements Visitor {
	
	private HashMap<String, Type> idType;
	private SymbolTable globalTable;
	private Vector<String> errList;
	
	
	public STableBuilder() {
		this.globalTable = new SymbolTable();
		this.errList = new Vector<String>();
		this.idType = new HashMap<String, Type>();
	}
	
	
	@Override
	public void visit(Display d) { }

	@Override
	public void visit(Or v) { }

	@Override
	public void visit(Program p) {
		p.m.accept(this);
		
		for(int i = 0; i < p.cl.size(); i++)
		{ 
			p.cl.get(i).accept(this);
		}
	}

	@Override
	public void visit(MainClass v) {
		SClassAnalyzer mainClass = new SClassAnalyzer("", v.i1.s);
		globalTable.setClass(mainClass);
	}

	@Override
	public void visit(ClassDeclSimple v) {
		SClassAnalyzer simpleClass = new SClassAnalyzer("", v.i.s);
		
		for(int i = 0; i < v.vl.size(); i++)
		{
			v.vl.get(i).accept(this);
			VariableAnalyzer auxVar = new VariableAnalyzer(v.vl.get(i).t, v.vl.get(i).i.s);
			if(!simpleClass.getCVariables().containsKey(auxVar.getName()))
				simpleClass.getCVariables().put(auxVar.getName(), auxVar);
			else
				errList.add("In the line of number " + v.vl.get(i).line_number + " this error occured: " + 
							   "The variable " + auxVar.getName() + 
							   " is already in the class " + simpleClass.getClassName() + ".");
		}
		
		for(int i = 0; i < v.ml.size(); i++)
		{
			v.ml.get(i).accept(this);
			SMethodAnalyzer auxMeth = new SMethodAnalyzer(v.ml.get(i).t, v.ml.get(i).i.s);
			
			if(!simpleClass.getClassMethods().containsKey(auxMeth.getName()))
				simpleClass.getClassMethods().put(auxMeth.getName(), auxMeth);
			else
				errList.add("In the line of number " + v.ml.get(i).line_number + " this error occured: " + 
							   "The method " + auxMeth.getName() + 
							   "is already in the class " + simpleClass.getClassName() + ".");	
		}
		
		if(!globalTable.getTable().containsKey(simpleClass.getClassName()))
			globalTable.setClass(simpleClass);
		else
			errList.add("In the line number " + v.line_number + " this error occured: " + 
						   "The class " + simpleClass.getClassName() + " already exists.");
	}

	@Override
	public void visit(ClassDeclExtends v) {
		SClassAnalyzer classeExtends = new SClassAnalyzer(v.j.s, v.i.s);
		
		for(int i = 0; i < v.vl.size(); i++)
		{
			n.vl.get(i).accept(this);
			VariableAnalyzer auxVar = new VariableAnalyzer(v.vl.get(i).t, v.vl.get(i).i.s);

			if(!classeExtends.getCVariables().containsKey(auxVar.getName()))
				classeExtends.getCVariables().put(auxVar.getName(), auxVar);
			else
				errList.add("In the line of number " + v.vl.get(i).line_number + " this error occured: " + 
							   "The variable " + auxVar.getName() + 
							   " is already in the class " + classeExtends.getClassName() + ".");
		}
		
		// Add methods for the class
		for(int i = 0; i < v.ml.size(); i++)
		{
			v.ml.get(i).accept(this);
			SMethodAnalyzer auxMeth = new SMethodAnalyzer(v.ml.get(i).t, v.ml.get(i).i.s);
			
			if(!classeExtends.getClassMethods().containsKey(auxMeth.getName()))
				classeExtends.getClassMethods().put(auxMeth.getName(), auxMeth);
			else
				errList.add("In the line of number " + v.ml.get(i).line_number + " this error occured: " + 
							   "The method " + auxMeth.getName() + 
							   "is already in the class " + classeExtends.getClassName() + ".");	
		}
		
		
		if(!globalTable.getTable().containsKey(classeExtends.getClassName())) {
			globalTable.setClass(classeExtends);
		}
		else
			errList.add("In the line number " + v.line_number + " this error occured: " + 
						   "The class " + classeExtends.getClassName() + " already exists.");
	
	}

	@Override
	public void visit(VarDecl vis) {
		idType.put(vis.i.s, vis.t);
		n.i.accept(this);
	}

	@Override
	public void visit(MethodDecl vis) {
		idType.put(vis.i.s, vis.t);

		SMethodAnalyzer auxMeth = new SMethodAnalyzer(vis.t, vis.i.s);
		VariableAnalyzer auxVar;
		
		for(int i = 0; i < vis.fl.size(); i++)
		{
			vis.fl.get(i).accept(this);
			auxVar = new VariableAnalyzer(vis.fl.get(i).t, vis.fl.get(i).i.s);
		
			if(!auxMeth.getArguments().containsKey(auxVar.getName()))
				auxMeth.getArguments().put(auxVar.getName(), auxVar);
			else
				errList.add("In the line of number " + vis.fl.get(i).line_number + " this error occured: " + 
						   "The argument variable " + auxVar.getName() + 
						   " is already in the method parameters " + auxMeth.getName() + ".");
		}
		
		
		for(int i = 0; i < vis.vl.size(); i++)
		{
			vis.vl.get(i).accept(this);
			auxVar = new VariableAnalyzer(vis.vl.get(i).t, vis.vl.get(i).i.s);
			
			if(!auxMeth.getLoacalVariables().containsKey(auxVar.getName()) && 
			   !auxMeth.getArguments().containsKey(auxVar.getName()))
				auxMeth.getLoacalVariables().put(auxVar.getName(), auxVar);
			else
				errList.add("In the line of number " + vis.vl.get(i).line_number 
						      + " this error occured: " 
						      + "The variable " + auxVar.getName() 
						      + " is already in the method local variables " + auxMeth.getName() + ".");
		}
		
		for (int i = 0; i < vis.sl.size(); i++) 
			vis.sl.get(i).accept(this);
		
		vis.e.accept(this);

	}

	@Override
	public void visit(Formal vis) {
		idType.put(vis.i.s, vis.t);
	}

	@Override
	public void visit(IntArrayType vis) { }

	@Override
	public void visit(BooleanType vis) { }

	@Override
	public void visit(IntegerType vis) { }

	@Override
	public void visit(IdentifierType vis) { }

	@Override
	public void visit(DoubleType vis) { }


	@Override
	public void visit(Block vis) { }

	@Override
	public void visit(If vis) {	}

	@Override
	public void visit(While vis) { }

	@Override
	public void visit(Print vis) { }

	@Override
	public void visit(Assign vis) {	}

	@Override
	public void visit(ArrayAssign vis) { }

	@Override
	public void visit(And vis) { }

	@Override
	public void visit(LessThan vis) { }

	@Override
	public void visit(Plus vis) { }

	@Override
	public void visit(Minus vis) { }

	@Override
	public void visit(Times vis) { }

	@Override
	public void visit(ArrayLookup vis) { }

	@Override
	public void visit(ArrayLength vis) { }
	

	@Override
	public void visit(Call vis) { }

	@Override
	public void visit(IntegerLiteral vis) { }
	

	@Override
	public void visit(True vis) { }

	@Override
	public void visit(False vis) { }

	@Override
	public void visit(IdentifierExp vis) { }

	@Override
	public void visit(This vis) { }

	@Override
	public void visit(NewArray vis) { }

	@Override
	public void visit(NewObject vis) { }

	@Override
	public void visit(Not vis) { }

	@Override
	public void visit(Identifier vis) {	}

	@Override
	public void visit(IfElse ifElse) { }

	@Override
	public void visit(GreaterThan gThan) { }


	public Vector<String> getErrList() {
		return errList;
	}


	public void setErrList(Vector<String> errList) {
		this.errList = errList;
	}


	public SymbolTable getTable() {
		return globalTable;
	}


	public void setTable(SymbolTable globalTable) {
		this.globalTable = globalTable;
	}


	public HashMap<String, Type> getIdType() {
		return idType;
	}


	public void setIdType(HashMap<String, Type> idType) {
		this.idType = idType;
	}

}