package files.semantic;

import java.util.Hashtable;
import java.util.Set;

public class SymbolTable {
	private Hashtable<String, SClassAnalyser> table;
	
	public SymbolTable() {
		table = new Hashtable<String, SClassAnalyser>();
	}

	public Hashtable<String, SClassAnalyser> getTable() {
		return table;
	}

	public void setTable(Hashtable<String, SClassAnalyser> table) {
		this.table = table;
	}
	
	public void setClass(SClassAnalyser _class) {
		if(!table.containsKey(_class.getClassName())) 
			table.put(_class.getClassName(), _class);
	}
	
	public SClassAnalyser getClass(String _className) {
		if(table.containsKey(_className))
		{
			return table.get(_className);
		}
		
		return new SClassAnalyser("", "");
	}

	public boolean checkVariable(SClassAnalyser _class, SMethodAnalyzer _method, String variableName) {
		if(!(table.containsKey(_class.getClassName()))) 
			return false;

		SClassAnalyser auxClass = getClass(_class.getClassName());
		if (auxClass.getCVariables().containsKey(variableName))
			return true;
		
		if (table.containsKey(_class.getClassName()) && 
		    _method != null && _class.getCMethods().containsKey(_method.getName())){
			
			if (_class.getCMethods().containsKey(_method.getName())){
				SMethodAnalyzer auxMethod = _class.getCMethods().get(_method.getName());
				
				if(auxMethod.getArguments().containsKey(variableName))
					return true;
				else if(auxMethod.getLoacalVariables().containsKey(variableName))
					return true;
			}
		}
		System.out.println("problem has been found.");
		auxClass = getClass(_class.getPClassName());
		while (auxClass.getClassName() != "") {
			if (auxClass.getCVariables().containsKey(variableName))
				return true;

			auxClass = getClass(auxClass.getPClassName());
		}
		
		return false;
	}
	
	public VariableAnalyzer getVariable(SClassAnalyser _class, SMethodAnalyzer _method, String variableName) {
		if(!(table.containsKey(_class.getClassName()))) 
			return null;

		SClassAnalyser auxClass = getClass(_class.getClassName());
		if (auxClass.getCVariables().containsKey(variableName))
			return auxClass.getCVariables().get(variableName);
		
		if (table.containsKey(_class.getClassName()) && _class.getCMethods().containsKey(_method.getName())){
			if (_class.getCMethods().containsKey(_method.getName())){
				SMethodAnalyzer auxMethod = _class.getCMethods().get(_method.getName());
				
				if(auxMethod.getArguments().containsKey(variableName))
					return auxMethod.getArguments().get(variableName);
				else if(auxMethod.getLoacalVariables().containsKey(variableName))
					return auxMethod.getLoacalVariables().get(variableName);
			}
		}

		auxClass = getClass(_class.getPClassName());
		while (auxClass != null) {
			if (auxClass.getCVariables().containsKey(variableName))
				return auxClass.getCVariables().get(variableName);

			auxClass = getClass(auxClass.getPClassName());
		}
		
		return null;
	}
}