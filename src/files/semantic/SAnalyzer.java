package files.semantic;

import java.util.*;

import AST.And;
import AST.ArrayAssign;
import AST.ArrayLength;
import AST.ArrayLookup;
import AST.Assign;
import AST.Block;
import AST.BooleanType;
import AST.Call;
import AST.ClassDeclExtends;
import AST.ClassDeclSimple;
import AST.Display;
import AST.DoubleType;
import AST.False;
import AST.Formal;
import AST.GreaterThan;
import AST.Identifier;
import AST.IdentifierExp;
import AST.IdentifierType;
import AST.If;
import AST.IfElse;
import AST.IntArrayType;
import AST.IntegerLiteral;
import AST.IntegerType;
import AST.LessThan;
import AST.MainClass;
import AST.MethodDecl;
import AST.Minus;
import AST.NewArray;
import AST.NewObject;
import AST.Not;
import AST.Or;
import AST.Plus;
import AST.Print;
import AST.Program;
import AST.This;
import AST.Times;
import AST.True;
import AST.Type;
import AST.VarDecl;
import AST.While;
import AST.Visitor.Visitor;

public class SAnalyzer implements Visitor {
	
	private Vector<String> errorList;
	private HashMap<String, Type> identifiersType;
	private SymbolTable sTable;
	private ClassAnalyzer currentClass;
	private MethodAnalyzer currentMethod;
	
	private Type analyzerT;
	
	
	public SAnalyzer() { /* ---------- changed ----------- */ 
		this.setSymbolTable(new SymbolTable());
		this.setErrorList(new Vector<String>());
		this.setIdentifiersType(new HashMap<String, Type>());
	}
	
	@Override
	public void visit(Display dis) { }

	@Override
	public void visit(Or or) {	}

	@Override
	public void visit(Program pro) {
		currentClass = sTable.getClass(pro.m.i1.s);
		currentMethod = null;
		pro.m.s.accept(this);

		for (int i = 0; i < pro.cl.size(); i++) {
			pro.cl.get(i).accept(this);
		}
	}

	@Override
	public void visit(MainClass mclass) {
		mclass.i1.accept(this);
		mclass.i2.accept(this);
		mclass.s.accept(this);
	}

	@Override
	public void visit(ClassDeclSimple vis) {
		currentClass = sTable.getClass(vis.i.s);
		vis.i.accept(this);
		for (int i = 0; i < vis.vl.size(); i++) 
			vis.vl.get(i).accept(this);

		for (int i = 0; i < vis.ml.size(); i++) 
			vis.ml.get(i).accept(this);
	}

	@Override
	public void visit(ClassDeclExtends cd) {
		currentClass = sTable.getClass(cd.i.s);
		currentClass.setParentClassName(cd.j.s);
		cd.i.accept(this);
		for (int i = 0; i < cd.vl.size(); i++)
			cd.vl.get(i).accept(this);

		if (!sTable.getTable().containsKey(cd.j.s))
			errorList.add("In the line of number " + cd.line_number + " this error occured: " + 
						"The class " + cd.j.s + " to be inherited does not exits.");

		for (int i = 0; i < cd.ml.size(); i++) 
			cd.ml.get(i).accept(this);
	}

	@Override
	public void visit(VarDecl vd) {
		if(!(vd.t instanceof BooleanType) 
		   && !(vd.t instanceof IntegerType)
		   && !(vd.t instanceof IdentifierType)
		   && !(vd.t instanceof IntArrayType)) {
			errorList.add("In the line of number " + vd.line_number + " this error occured: " + 
					"The type was not declared.");
		}
	}

	@Override
	public void visit(MethodDecl md) {
		currentMethod = sTable.getClass(currentClass.getClassName()).getClassMethods().get(n.i.s);
		
		for (int i = 0; i < md.fl.size(); i++) 
			md.fl.get(i).accept(this);
		
		for (int i = 0; i < md.vl.size(); i++) 
			md.vl.get(i).accept(this);
		
		for (int i = 0; i < md.sl.size(); i++) 
			md.sl.get(i).accept(this);
		
		md.e.accept(this);
		

	}

	@Override
	public void visit(Formal n) { }

	@Override
	public void visit(IntArrayType n) {	}

	@Override
	public void visit(BooleanType n) {	}

	@Override
	public void visit(IntegerType n) {	}

	@Override
	public void visit(IdentifierType n) { }
	

	@Override 
	public void visit(DoubleType n) { }

	@Override
	public void visit(Block bl) {
		for (int i = 0; i < bl.sl.size(); i++)
			bl.sl.get(i).accept(this);
	}

	@Override
	public void visit(If n) {
		n.e.accept(this);
	}

	@Override
	public void visit(While n) {
		n.e.accept(this);
	}

	@Override
	public void visit(Print n) {
		n.e.accept(this);

	}

	@Override
	public void visit(Assign n) { }		
	

	@Override
	public void visit(ArrayAssign aa) {
		aa.e1.accept(this);
		Type type1 = analyzerT;
		
		aa.e2.accept(this);
		Type type2 = analyzerT;
		
		if (!(type1 instanceof IntegerType && type2 instanceof IntegerType)) {
			System.out.println("Type 1 " + type1 + " Type 2 " + type2);
			errorList.add("In the line of number " + aa.line_number + " this error occured: " + 
					"The expressions in ArrayAssign are not IntArrayType or Integer.");
		}

	}

	@Override
	public void visit(And n) { }

	@Override
	public void visit(LessThan lt) {
		lt.e1.accept(this);
		Type type1 = analyzerT;
		
		lt.e2.accept(this);
		Type type2 = analyzerT;
		
		if (!(type1 instanceof IntegerType && type2 instanceof IntegerType)) {
			errorList.add("In the line of number " + lt.line_number + " this error occured: " + 
					"The expressions in LessThan are not integers.");
		}


	}

	@Override
	public void visit(Plus pl) {
		pl.e1.accept(this);
		Type type1 = analyzerT;
		
		pl.e2.accept(this);
		Type type2 = analyzerT;
		
		if (!(type1 instanceof IntegerType && type2 instanceof IntegerType)) {
			errorList.add("In the line of number " + pl.line_number + " this error occured: " + 
					"The expressions in Plus are not integers.");
		}
	}

	@Override
	public void visit(Minus mn) {
		mn.e1.accept(this);
		Type type1 = analyzerT;
		
		mn.e2.accept(this);
		Type type2 = analyzerT;
		
		if (!(type1 instanceof IntegerType && type2 instanceof IntegerType)) {
			System.out.println("Type 1 " + type1 + " Type 2 " + type2);
			errorList.add("In the line of number " + mn.line_number + " this error occured: " + 
					"The expressions in Minus are not integers.");
		}
	}

	@Override
	public void visit(Times tm) {
		tm.e1.accept(this);
		Type type1 = analyzerT;
		
		tm.e2.accept(this);
		Type type2 = analyzerT;
		
		if (!(type1 instanceof IntegerType && type2 instanceof IntegerType)) {
			errorList.add("In the line of number " + tm.line_number + " this error occured: " + 
					"The expressions in Times are not integers.");
		}
	}

	@Override
	public void visit(ArrayLookup al) {
		al.e1.accept(this);
		Type type1 = analyzerT;
		al.e2.accept(this);
		Type type2 = analyzerT;
		
		if (!(type1 instanceof IntegerType && type2 instanceof IntegerType))
			errorList.add("In the line of number " + al.line_number + " this error occured: " + 
					"The expressions in ArrayLookup are not integers.");

	}

	@Override
	public void visit(ArrayLength al) {
		al.e.accept(this);
		
		Type type = analyzerT;
		
		if(!(type instanceof IntegerType))
			errorList.add("In the line of number " + al.line_number + " this error occured: " + 
					"The expressions in ArrayLength is not integer.");
	}

	@Override
	public void visit(Call cl) {	}

	@Override
	public void visit(IntegerLiteral il) { }

	@Override
	public void visit(True t) { }
	

	@Override
	public void visit(False fl) { }

	@Override
	public void visit(IdentifierExp ie) {
		analyzerT = identifiersType.get(ie.s);
	}

	@Override
	public void visit(This n) { }
	

	@Override
	public void visit(NewArray n) {
		n.e.accept(this);

	}

	@Override
	public void visit(NewObject no) {
		if (!this.getSymbolTable().getTable().containsKey(no.i.s)) {
			errorList.add("In the line of number " + no.line_number + " this error occured: " + 
					   "The identifier " + no.i.s + " does not have a corresponding class previously declared.");
		}
	}

	@Override
	public void visit(Not nt) { }
	

	@Override
	public void visit(Identifier n) { }

	@Override
	public void visit(IfElse ifElse) { }

	@Override
	public void visit(GreaterThan greaterThan) { }

	public SymbolTable getSymbolTable() {
		return sTable;
	}

	public void setSymbolTable(SymbolTable sTable) {
		this.sTable = sTable;
	}

	public Vector<String> getErrorList() {
		return errorList;
	}

	public void setErrorList(Vector<String> errorList) {
		this.errorList = errorList;
	}

	public HashMap<String, Type> getIdentifiersType() {
		return identifiersType;
	}

	public void setIdentifiersType(HashMap<String, Type> identifiersType) {
		this.identifiersType = identifiersType;
	}

}