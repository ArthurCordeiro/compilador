package files.semantic;

import java.util.Hashtable;

import AST.Type;

public class SMethodAnalyzer { /* ---------- changed ----------- */ 
	
	private String name;
	private Type type;
	
	
	private Hashtable<String, VariableAnalyzer> arguments;
	private Hashtable<String, VariableAnalyzer> localVariables;
	
	
	public SMethodAnalyzer(Type type, String name) { /* ---------- changed ----------- */ 
		this.setType(type);
		this.setName(name);
		this.setLocalVariables(new Hashtable<String, VariableAnalyzer>());
		this.setArguments(new Hashtable<String, VariableAnalyzer>());
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public void setName(String name) {
		this.name = name;
	}	

	public String getName() {
		return name;
	}

	public void setLocalVariables(Hashtable<String, VariableAnalyzer> localVariables) {
		this.localVariables = localVariables;
	}

	public Hashtable<String, VariableAnalyzer> getLocalVariables() {
		return localVariables;
	}

	public void setArguments(Hashtable<String, VariableAnalyzer> arguments) {
		this.arguments = arguments;
	}
	
	public Hashtable<String, VariableAnalyzer> getArguments() {
		return arguments;
	}

}