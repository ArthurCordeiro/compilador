package files.semantic;

import java.util.Hashtable;

public class SClassAnalyzer { /* ------ changed -------- */
	private String pClassName; //parent Class name
	private String className; 
	
	private Hashtable<String, VariableAnalyzer> classVariables;
	private Hashtable<String, MethodAnalyzer> classMethods;
	
	public SClassAnalyzer(String pClassName, String className) { /* ---------- changed ----------- */ 
		this.setClassName(className);
		this.setPClassName(pClassName);
		this.setClassVariables(new Hashtable<String, VariableAnalyzer>());
		this.setClassMethods(new Hashtable<String, MethodAnalyzer>());
	}

	public String getPClassName() { /* ---------- changed ----------- */ 
		return pClassName;
	}

	public void setPClassName(String pClassName) {  /* ---------- changed ----------- */ 
		this.pClassName = pClassName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Hashtable<String, VariableAnalyzer> getCVariables() {  /* ---------- changed ----------- */ 
		return classVariables;
	}

	public void setCVariables(Hashtable<String, VariableAnalyzer> classVariables) {  /* ---------- changed ----------- */ 
		this.classVariables = classVariables; 
	}

	public Hashtable<String, MethodAnalyzer> getCMethods() { /* ---------- changed ----------- */ 
		return classMethods;
	}

	public void setCMethods(Hashtable<String, MethodAnalyzer> classMethods) { /* ---------- changed ----------- */ 
		this.classMethods = classMethods;
	}

}